SHELL := /bin/bash
.SHELLFLAGS := -O extglob -c
TEXINPUTS := ${TEXINPUTS}
NAME=presentation

$(NAME).pdf:
	cd src && TEXINPUTS=${TEXINPUTS}:figures/ latexmk -time -bibtex -pdf -lualatex $(NAME).tex

$(NAME).bbl: $(NAME).pdf

arxiv: $(NAME).bbl
	mkdir -p arXiv
	cp src/*.{tex,bbl} arXiv/
	cp src/figures/* arXiv/
	tar -cvf arXiv.tar -C arXiv .
	gzip -f arXiv.tar
	rm -rf arXiv

clean:
	rm src/*.!(bib|tex)

